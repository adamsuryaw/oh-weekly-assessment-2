let sum = 0;
let x = 0;
const sumArray = (input) => {
    console.log(input);
    for(let i = 0; i < input.length; i++) {
        // console.log("ini i " + i)
        if(i == 0) {
            sum = 1;
            x = 1;
        } else {
            sum = input[i] + x;
            // console.log("ini sum " + sum);
            x = sum;
            // console.log("ini x " + x);
        }
        
    }
    return x;
};

// input test
const input1 = [1, 2, 5, 8, 9, 10];
const input2 = [1, 2, 3, 4, 5];

sumArray(input1) // output: 35
sumArray(input2) // output: 15
  