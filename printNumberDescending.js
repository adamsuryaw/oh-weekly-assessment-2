const printNumberDescending = (input) => {
    for(let i = input; i >= 1; i--) {
        console.log(i);
    }
    return;
  };
  
// input test
const input1 = 5;
const input2 = 10;
  
printNumberDescending(input2);