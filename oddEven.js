const oddEven = (input) => {
    if(input % 2 == 0) {
        return `${input} is even number`;
    } else {
        return `${input} is odd number`;
    }
}

const input1 = 3;
const input2 = 10;

console.log(oddEven(input1));
