const oddEvenArray = (input) => {
    for(let i = 0; i < input.length; i++) {
        if(input[i] % 2 == 0) {
            console.log(`${input[i]} is even number`);
        } else {
            console.log(`${input[i]} is odd number`);
        }
    }
    return;
  };
  
// input test
const input1 = [1, 4, 6, 3, 10, 7];
  
oddEvenArray(input1);
  
  